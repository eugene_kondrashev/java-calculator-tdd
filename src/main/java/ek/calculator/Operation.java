package ek.calculator;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Calculator operation.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 */
public interface Operation<T> {

    T result();

    class Sum implements Operation<Integer> {

        private final Stream<Integer> arguments;

        public Sum(String args) {
            this(Arrays.stream(args.split(",")).map(Integer::valueOf));
        }

        public Sum(Stream<Integer> args) {
            this.arguments = args;
        }
        public Integer result() {
            return this.arguments.mapToInt(i->i.intValue()).sum();
        }
    }
}
