package ek.calculator;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

/**
 * Doc.
 *
 * @author Eugene Kondrashev (eugene.kondrashev@gmail.com)
 * @version $Id$
 * @since 0.3
 */
public class SumTest {

    @Test
    public void acceptsCommaSeparatedStringOfArguments() {
        MatcherAssert.assertThat(
            new Operation.Sum("1,2,3").result(),
            Matchers.equalTo(6)
        );
    }
}
